const express = require("express")
const hbs = require("hbs")
const fetch = require("node-fetch")
let app = express()
app.set('view engine', 'hbs')
app.get('/', (req, res) => {
    res.send("Hello, express")
})
app.get('/weather/(:city)?', async function (req, res) {
    let city = req.params.city
    try {
        fetch('http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=31d1a9a3acdde31da3e1aa8ae169b8aa')
            .then(res => res.json())
            .then(data => {
                let mytemp = Math.floor(data.main.temp - 273.15)
                let pres = data.main.pressure
                let humi = data.main.humidity

                let weather = data.weather[0].icon
                const icon_night = weather.substring(0, weather.length - 1) + 'n'
                const icon_day = weather.substring(0, weather.length - 1) + 'd'

                var icon;
                if (icon_night === weather) {
                    icon = icon_night
                } else if (icon_day === weather) {
                    icon = icon_day
                }

                res.render('weather.hbs', {icon, mytemp, pres, humi, city})
            })

    } catch (e) {
        console.log(e)
        res.send("ERROR")
    }
})


app.get('/weather', (req, res) => {

    try {
        res.render('weather.hbs')
    } catch (e) {
        console.log(e)
        res.send("ERROR")
    }
})

app.listen(3000, () => {
    console.log("Example app listening on port 3000")
})