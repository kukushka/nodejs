require('should')
const mongoose = require('mongoose')
const supertest = require('supertest')
const app = require('../app')

const request = supertest(app)

const user1 = { name: 'User1', email: 'user1@gmail.com', age: 21, password: '12345678' }
const user2 = { name: 'User2', email: 'user2@gmail.com', age: 22, password: '12345678' }

const task1 = { description: 'Some description' }
const task2 = { description: 'Some description 2' }
const task3 = { description: 'Some description 3', completed: true }

describe('user-task flow', () => {
  let authToken

  before(() => require('../db/mongoose'))

  after(async () => {
    await Promise.all(Object.values(mongoose.models).map(model => model.collection.drop()))
    await mongoose.connection.close()
  })

  it('should fail User1 registration', done => {
    request
      .post('/users')
      .send({ ...user1, email: undefined })
      .expect(400)
      .end((err, res) => {
        if (err) return done(err)

        res.body.should.have.property('error').which.is.not.empty().String()
        done()
      })
  })

  it('should succeed User1 registration', done => {
    request
      .post('/users')
      .send(user1)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err)

        const { user, token } = res.body

        user.should.have.property('_id').which.is.not.empty().String()
        token.should.be.String().not.empty()

        done()
      })
  })

  it('should succeed User2 registration', done => {
    request
      .post('/users')
      .send(user2)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err)

        const { user, token } = res.body

        user.should.have.property('_id').which.is.not.empty().String()
        token.should.be.String().not.empty()

        done()
      })
  })

  it('should login User1', done => {
    request
      .post('/users/login')
      .send({ email: user1.email, password: user1.password })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)

        const { user, token, message } = res.body

        user.should.have.property('_id').which.is.not.empty().String()
        token.should.be.String().not.empty()
        message.should.equal('success')

        authToken = `Bearer ${token}`
        done()
      })
  })

  it('should create Task1', done => {
    request
      .post('/tasks')
      .send(task1)
      .set('Authorization', authToken)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err)

        res.body.should.have.property('_id').which.is.String().not.empty()
        task1._id = res.body._id

        done()
      })
  })

  it('should create Task2', done => {
    request
      .post('/tasks')
      .send(task2)
      .set('Authorization', authToken)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err)

        res.body.should.have.property('_id').which.is.String().not.empty()
        task2._id = res.body._id

        done()
      })
  })

  it('should get User1\'s tasks', done => {
    request
      .get('/tasks')
      .set('Authorization', authToken)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)

        res.body.should.be.Array().with.length(2)
        done()
      })
  })

  it('should get User1\'s task by id', done => {
    request
      .get(`/tasks/${task1._id}`)
      .set('Authorization', authToken)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)

        res.body.should.have.properties('description', 'completed')
        done()
      })
  })

  it('should logout User1', done => {
    request
      .post('/users/logout')
      .set('Authorization', authToken)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)

        res.body.message.should.equal('logout success')

        authToken = null
        done()
      })
  })

  it('should login User2', done => {
    request
      .post('/users/login')
      .send({ email: user2.email, password: user2.password })
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)

        const { token, message } = res.body

        message.should.equal('success')

        authToken = `Bearer ${token}`
        done()
      })
  })

  it('should create Task3', done => {
    request
      .post('/tasks')
      .send(task3)
      .set('Authorization', authToken)
      .expect(201)
      .end((err, res) => {
        if (err) return done(err)

        res.body.should.have.property('_id').which.is.String().not.empty()
        task3._id = res.body._id

        done()
      })
  })

  it('should get User2\'s tasks', done => {
    request
      .get('/tasks')
      .set('Authorization', authToken)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)

        res.body.should.be.Array().with.length(1)
        done()
      })
  })

  it('should fail get User1\'s task by id if logged in as User2', done => {
    request
      .get(`/tasks/${task1._id}`)
      .set('Authorization', authToken)
      .expect(404)
      .end((err, res) => {
        if (err) return done(err)

        res.body.error.should.equal('not found')
        done()
      })
  })

  it('should logout User2', done => {
    request
      .post('/users/logout')
      .set('Authorization', authToken)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)

        res.body.message.should.equal('logout success')

        authToken = null
        done()
      })
  })

  it('should fail get User1\'s task by id if not logged in', done => {
    request
      .get(`/tasks/${task1._id}`)
      .set('Authorization', authToken)
      .expect(403)
      .end((err, res) => {
        if (err) return done(err)

        res.body.error.should.equal('Forbidden Access')
        done()
      })
  })
})
