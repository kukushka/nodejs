const express = require('express')
const router = express.Router()
const Task = require('../models/task')
const auth = require('../middleware/auth')

router.get('/', auth, async (req, res) => {
  try {
    const tasks = await Task.find({ owner: req.user.id })
    res.status(200).json(tasks)
  } catch (e) {
    res.status(400).end()
  }
})

router.get('/:id', auth, async (req, res) => {
  try {
    const { id } = req.params
    const task = await Task.findOne({ _id: id, owner: req.user.id })
    if (!task) return res.status(404).json({ error: 'not found' })

    res.status(200).json(task)
  } catch (e) {
    res.status(400).end()
  }
})

router.post('/', auth, async (req, res) => {
  try {
    const task = await new Task({ ...req.body, owner: req.user.id }).save()
    res.status(201).json(task)
  } catch (e) {
    res.status(400).end()
  }
})

router.put('/:id', auth, async (req, res) => {
  try {
    const { id } = req.params
    const task = await Task.findOneAndUpdate({ _id: id, owner: req.user.id }, req.body, { new: true })
    if (!task) return res.status(404).end()

    res.status(200).json(task)
  } catch (e) {
    res.status(400).end()
  }
})

router.delete('/:id', auth, async (req, res) => {
  try {
    const { id } = req.params
    const task = await Task.findOne({ _id: id, owner: req.user.id })
    if (!task) return res.status(404).end()

    await task.delete()
    res.status(200).end()
  } catch (e) {
    res.status(400).end()
  }
})

router.delete('/', auth, async (req, res) => {
  try {
    await Task.deleteMany({ owner: req.user.id })
    res.status(200).end()
  } catch (e) {
    res.status(400).end()
  }
})

module.exports = router
