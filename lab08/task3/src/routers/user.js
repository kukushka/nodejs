const express = require('express')
const router = express.Router()
const User = require('../models/user')
const auth = require('../middleware/auth')

router.get('/', async (req, res) => {
  try {
    const users = await User.find({})
    res.status(200).json(users)
  } catch (e) {
    res.status(400).end()
  }
})

router.get('/me', auth, (req, res) => {
  res.status(200).json(req.user)
})

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params
    const user = await User.findById(id)
    if (!user) return res.status(404).end()

    res.status(200).json(user)
  } catch (e) {
    res.status(400).end()
  }
})

router.post('/', async (req, res) => {
  try {
    const user = await new User(req.body).save()
    const token = await user.generateAuthToken()

    res.status(201).json({ user, token })
  } catch (e) {
    res.status(400).json({ error: e.message })
  }
})

router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params
    const user = await User.findById(id)
    if (!user) return res.status(404).end()

    const updates = ['name', 'email', 'password', 'age']
    updates.forEach(update => user[update] = req.body[update])

    await user.save()
    res.status(200).json(user)
  } catch (e) {
    res.status(400).end()
  }
})

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params
    const user = await User.findById(id)
    if (!user) return res.status(404).end()

    await user.delete()
    res.status(200).end()
  } catch (e) {
    res.status(400).end()
  }
})

router.delete('/', async (req, res) => {
  try {
    await User.deleteMany({})
    res.status(200).end()
  } catch (e) {
    res.status(400).end()
  }
})

router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body

    const user = await User.findOneByCredentials(email, password)
    const token = await user.generateAuthToken()

    res.status(200).json({ user, token, message: 'success' })
  } catch (e) {
    res.status(400).json({ error: e.message })
  }
})

router.post('/logout', auth, async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter(({ token }) => {
      return token !== req.token
    })

    await req.user.save()
    res.status(200).json({ message: 'logout success' })
  } catch (e) {
    res.status(500).end()
  }
})

module.exports = router
