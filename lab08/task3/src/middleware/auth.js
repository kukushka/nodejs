const jwt = require('jsonwebtoken')
const User = require('../models/user')

const auth = async (req, res, next) => {
  try {
    const token = req.get('Authorization').replace('Bearer ', '')
    const decoded = jwt.verify(token, 'sad21dsaaxcslASddd')
    const user = await User.findOne({ _id: decoded._id, 'tokens.token': token })
    if(!user) throw new Error()

    req.user = user
    req.token = token

    next()
  } catch (e) {
    res.status(403).json({ error: 'Forbidden Access' })
  }
}

module.exports = auth
