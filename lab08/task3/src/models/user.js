const mongoose = require('mongoose')
const Validator = require('validator')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  age: {
    type: Number,
    validate: (value) => {
      if (value < 0) throw new Error('Age must be a positive number')
    },
    default: 0,
  },
  email: {
    type: String,
    required: true,
    trim: true,
    validate: (value) => {
      if (!Validator.isEmail(value)) throw new Error('Email is invalid')
    },
    transform: (value) => value.toLowerCase(),
    unique: true,
  },
  password: {
    type: String,
    required: true,
    trim: true,
    validate: (value) => {
      if (value.includes('password') || value.length <= 7) {
        throw new Error('Password can\'t contain word \'password\' and must be less than or equal to 7')
      }
    },
  },
  tokens: [{
    token: {
      type: String,
      required: true,
    },
  }],
})

userSchema.pre('save', async function(next) {
  if (this.isModified('password')) this.password = await bcrypt.hash(this.password, 8)
  next()
})

userSchema.statics.findOneByCredentials = async function(email, password) {
  const user = await this.findOne({ email })
  if (!user) throw new Error('Incorrect email')

  const isMatch = await bcrypt.compare(password, user.password)
  if (!isMatch) throw new Error('Incorrect password')

  return user
}

userSchema.methods.generateAuthToken = async function() {
  const token = jwt.sign({ _id: this._id.toString() }, 'sad21dsaaxcslASddd')
  this.tokens = [...this.tokens, { token }]
  await this.save()
  return token
}

userSchema.methods.toJSON = function() {
  const { password, tokens, ...user } = this.toObject()
  return user
}

userSchema.virtual('tasks', {
  ref: 'Task',
  localField: '_id',
  foreignField: 'owner',
})

const User = mongoose.model('User', userSchema)

module.exports = User
