const mongoose = require('mongoose')
const dbConfig = require('../config/db')

module.exports = mongoose
  .connect(dbConfig.uri, { useNewUrlParser: true })
  .catch(e => console.log(e))
