const Task = require('../models/task');
const express = require('express');
const router = express.Router();

router.post("/tasks", async (req, res) => {
    // const email = req.body.email;
    const task = new Task({
        description: req.body.description,
        completed: req.body.completed,
    });


    try {
        await task.save();
        res.status(201).json(task);

    } catch(error) {
        res.status(500).send(error.message);

    }


});

router.get("/tasks(/:id)?", async (req, res) => {
    if(req.params.id) {
        try {
            const oneTask = await Task.findById(req.params.id);
            res.status(201).json(oneTask);
        } catch(error) {
            res.status(500).send(error.message);
        }
    } else {
        try {
            const tasks = await Task.find();
            res.status(201).json(tasks);
        } catch(error) {
            res.status(500).send(error.message);
        }
    }

});

router.get("/tasks/delete/:id", async (req, res) => {
    try {
        const oneTask = await Task.deleteOne({ _id: req.params.id });
        res.status(201).send("Successfully deleted");
    } catch(error) {
        res.status(500).send(error.message);
    }

});

module.exports = router