const User = require('../models/user');
const express = require('express');
const router = express.Router();

router.post("/users", async (req, res) => {
    const email = req.body.email;
    const user = new User({
        name: req.body.name,
        surname: req.body.surname,
        age: req.body.age,
        email: req.body.email,
        password: req.body.password
    });

    let userCheck = await User.findOne({ email });
    if (userCheck) {
        return res.status(400).send("Email already registered.");
    }
    else {
        try {
            await user.save();
            res.status(201).json(user);

        } catch(error) {
            res.status(500).send(error.message);

        }
    }


});

router.get("/users(/:id)?", async (req, res) => {
    if(req.params.id) {
        try {
            const oneUser = await User.findById(req.params.id);
            res.status(201).json(oneUser);
        } catch(error) {
            res.status(500).send(error.message);
        }
    } else {
        try {
            const users = await User.find();
            res.status(201).json(users);
        } catch(error) {
            res.status(500).send(error.message);
        }
    }
});

router.get("/users/delete/:id", async (req, res) => {
    try {
        const oneUser = await User.deleteOne({ _id: req.params.id });
        res.status(201).send("Successfully deleted");
    } catch(error) {
        res.status(500).send(error.message);
    }

});




module.exports = router