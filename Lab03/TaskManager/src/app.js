const mongoose = require('mongoose');
const express = require('express');

require('./db/mongoose');


const userRouter = require('./routes/user');
const User = require('./models/user');

const taskRouter = require('./routes/task');
const Task = require('./models/task');

const app = express();

app.use(express.json())
app.use(userRouter);
app.use(taskRouter);


app.listen(3000, () => {
    console.log('Server is listening on port 3000');
});
