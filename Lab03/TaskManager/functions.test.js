const functions = require('./modules/functions');
const assert = require('assert');
const request = require('request')
const User = require('./models/user');

describe('factorial', function () {
    it('F(0) повинен бути 1', function () {
        assert.equal(functions.factorial(0), 1)
    })
    it('F(-5) повинен бути -120', function () {
        assert.equal(functions.factorial(-5), -120)
    })
    it('F(6) повинен бути 720', function () {
        assert.equal(functions.factorial(6), 720)
    })
})

let users = [
    {name: 'Sasha', age: 20, email: 'kolos20@gmail.com'}, // без помилок
    {name: 'Taylor'}, // помилка, не вказані значення age та email
    {name: 'Q', age: 15, email: 'qwerty20@gmail.com'}, // помилка в name (мало символів)
    {name: 'Kate', age: 24, email: 'kate.com'}, // помилка в email
    {name: 'Susan', age: 'a', email: 'susan10@gmail.com'}, // помилка в age (не число)
    {name: 'Jessica', age: -1, email: 'jessica10@gmail.com'}, // помилка в age (менше 0)
    {name: 'Vasyl', age: 10, email: 'vasyl10@gmail.com'}, // без помилок

]

beforeEach(async function () {
    await User.deleteMany();
});

describe('User', function () {
    for (let i = 0; i < users.length; i++) {
        it(`User ${i + 1} is testing`, function (done) {
            let user = new User({name: users[i].name, email: users[i].email, age:users[i].age});
            user.save(done);
        })
    }
})

























// describe('Tests for Task Manager', function () {
//
//         it('1 CreateNewUserWithErrorValidation', (done) => {
//
//             var options = {
//                 'method': 'POST',
//                 'url': 'http://localhost:3000/users?name=kate1555&password=12345678&age=18&email=kate1555gmail.com&admin=true',
//                 'headers': {
//                     'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//                 }
//             };
//             request(options, function (error, response) {
//                 if (error) throw new Error(error);
//                 console.log(response.body);
//                 let temp = JSON.parse(response.body);
//                 console.log(temp.errors["email"].reason.message);
//                 console.log(temp.errors["email"].reason.statusCode);
//                 if (temp.errors["email"].reason.statusCode === 403 && temp.errors["email"].reason.message == 'Email невірний') {
//                     done();
//                 } else {
//                     done(response.body.statusCode)
//                 }
//             });
//     })
//     it('2 CreateNewUser1', (done) => {
//
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/users?name=user1&password=12345678&age=18&email=user1@gmail.com&admin=true',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//
//         });
//     })
//
//     it('3 CreateNewUser2', (done) => {
//
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/users?name=user2&password=12345678&age=18&email=user2@gmail.com&admin=true',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//
//         });
//     })
//
//     it('4 LoginUser1', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/users/login?password=12345678&email=user1@gmail.com',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYyOTh9.62qovW61f3r3MZkj3AyqmbAi8TG9OHXrmWb9wmfn1Gg'
//             },
//             formData: {
//
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//     })
//
//     it('5 AddTask1', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/tasks?description=task1&completed=true',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//     })
//
//     it('6 AddTask2', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/tasks?description=taks2&completed=true',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//     })
//
//     it('7 GetTasksUser1', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'GET',
//             'url': 'http://localhost:3000/tasks',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//     })
//
//     it('8 ChangeTask1ById', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'PUT',
//             'url': 'http://localhost:3000/tasks?description=abc&completed=true&id=62c485726cc8321c306f1474',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//
//     })
//
//     it('9 LogoutUser1', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/users/logout',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0NDkzNWEyNWNkYTI3ZTA4YzllNjciLCJpYXQiOjE2NTcwMzI4MjJ9.wc5YZGnRJq9W0qbslFnKRHiok94uNQ7v4eVign15S88'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//
//
//     })
//
//     it('10 LoginUser2', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/users/login?password=12345678&email=user2@gmail.com',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYyOTh9.62qovW61f3r3MZkj3AyqmbAi8TG9OHXrmWb9wmfn1Gg'
//             },
//             formData: {
//
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//     })
//
//     it('11 AddTask3', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/tasks?description=taks3&completed=true',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//     })
//
//     it('12 GetTasksUser2', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'GET',
//             'url': 'http://localhost:3000/tasks',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//     })
//
//     it('13 ChangeTask1ById', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'PUT',
//             'url': 'http://localhost:3000/tasks?description=abc&completed=true&id=62c485726cc8321c306f1474',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 404) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//
//     })
//
//
//     it('14 LogoutUser2', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'POST',
//             'url': 'http://localhost:3000/users/logout',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0NDkzNWEyNWNkYTI3ZTA4YzllNjciLCJpYXQiOjE2NTcwMzI4MjJ9.wc5YZGnRJq9W0qbslFnKRHiok94uNQ7v4eVign15S88'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 200) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//
//
//     })
//
//     it('15 ChangeTask1ById', (done) => {
//
//         var request = require('request');
//         var options = {
//             'method': 'PUT',
//             'url': 'http://localhost:3000/tasks?description=abc&completed=true&id=62c485726cc8321c306f1474',
//             'headers': {
//                 'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MmM0ODUxOTZjYzgzMjFjMzA2ZjE0NzEiLCJpYXQiOjE2NTcwNDYzMTB9.I-9VQb-WjvZEy3C6qYsS9zNJCpLpceGxYLSubSY-OCs'
//             }
//         };
//         request(options, function (error, response) {
//             if (error) throw new Error(error);
//             console.log(response.body);
//             if (response.statusCode === 403) {
//                 done();
//             } else {
//                 done(response.body.statusCode)
//             }
//         });
//
//
//     })
//
//
//
//
//
// })
//






