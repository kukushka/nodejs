const lodash = require("lodash");

let m = lodash.max([125,148,452,65])
console.log(m)

let p = lodash.last([12, 25, 3]);
console.log(p)

let c = lodash.xor([2, 1], [2, 3], [32, 13], [22, 13]);
console.log(c)

let r = lodash.size('perfergebbrrgles');
console.log(r)

let y = lodash.upperCase('--foo-bar');
console.log(y)